// client.h
// client class header file

#ifndef CLIENT_H
#define CLIENT_H

#include <SDL/SDL_net.h>

class Client {
	public:
		Client();
		~Client();
		
		unsigned int sendData(const void*, unsigned int);
		unsigned int receiveData(void*, unsigned int, unsigned int);
		bool connect(const char*, unsigned int);
		void disconnect();
		void getMessages();
		
	private:
		TCPsocket serverSocket_;
		SDLNet_SocketSet socketSet_;
		char* messageBuffer_;
		unsigned int messageBufferLen_;
		bool isConnected_;
		
};

#endif // CLIENT_H
