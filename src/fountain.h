// fountain.h
// fountain class header file

#ifndef FOUNTAIN_H
#define FOUNTAIN_H

#include "object.h"

class Fountain : public Object {
	public:
		Fountain();
		Fountain(unsigned int, unsigned int);
		
		virtual void draw();
		virtual void activate();
		virtual void tick();
		
		unsigned int getFlowRate();
		void setFlowRate(unsigned int flowRate);
		
	private:
		unsigned int flowRate_;
		unsigned char fountainNo_;
		static unsigned char nFountains_;
		
};

#endif // FOUNTAIN_H
