// wizard.h 
// wizard class prototype header.

#ifndef WIZARD_H 
#define WIZARD_H

#include <SDL/SDL.h>

#include "object.h"

class Wizard : public Object {
	public:
		Wizard();
		Wizard(unsigned int, unsigned int);
		~Wizard();
		
		virtual void draw();
		virtual void activate();
		virtual void tick();
		
		unsigned char** findShortestPaths();
		
		virtual void move(unsigned char moveDirection);
		
		static unsigned int nWizards;
		
	private:
		bool isAlive_;
		unsigned int speed_;
		
		void loadImages();
		void unloadImages();
		
		unsigned char moveDirection_;
		unsigned char faceDirection_;
		unsigned int steps_;
		SDL_Surface* south_;
		SDL_Surface* north_;
		SDL_Surface* west_;
		SDL_Surface* east_;
		
		SDL_Surface* walkSouth_ [8];
		SDL_Surface* walkNorth_ [8];
		SDL_Surface* walkWest_ [8];
		SDL_Surface* walkEast_ [8];
};

#endif // WIZARD_H
