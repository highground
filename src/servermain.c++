#include <iostream>
#include <SDL/SDL.h>
#include <assert.h>

#include "fountain.h"
#include "highground.h"
#include "map.h"
#include "object.h"
#include "server.h"
#include "spells.h"
#include "wizard.h"
#include "msg.h"

Map* map;
Server* server;

Object **objects = NULL;
unsigned int nObjects = 0;

int main(int argc, char *argv[]);

int main(int argc, char *argv[]) {
	if( SDL_Init(SDL_INIT_TIMER) < 0 ) {
		std::cout << "Unable to initialise SDL : "
				  << SDL_GetError() << std::endl;
		exit(1);
	}
	atexit(SDL_Quit);
	std::cout << "hgserver: initialising SDLNet" << std::endl;
	SDLNet_Init();
	atexit(SDLNet_Quit);

	map = new Map();
	map->setSize(128, 128);
	map->initialise();
	map->subdivideDisplace(100, 0.7, 6);

	std::cout << "hgserver: establishing server socket" << std::endl;
	server = new Server(0x1234, 4);
	std::cout << "hgserver: awaiting data" << std::endl;
	unsigned int ip;
	while (1) {
		while (server->addClient() != 0) {
			ip = server->getClientIp(server->getNClients() - 1);
			std::cout << "hgserver: "
			          << ((ip >> 0) & 0xff) << "."
			          << ((ip >> 4) & 0xff) << "."
			          << ((ip >> 8) & 0xff) << "."
			          << ((ip >> 12) & 0xff) << ":"
			          << server->getClientPort(server->getNClients() - 1)
			          << " connected" << std::endl;
			sendMap(server->getNClients() - 1);
		}
		server->getMessages();
#if 0
		while (server.getNActiveSockets(10) != 0) {
			for (client = 0; client < server.getNClients(); ++client) {
				if (server.receiveData(data, client, 0x100) != 0)
					break;
			}
			if (client < server.getNClients()) {
				ip = server.getClientIp(client);
				std::cout << "hgserver: "
				          << ((ip >> 0) & 0xff) << "."
				          << ((ip >> 4) & 0xff) << "."
				          << ((ip >> 8) & 0xff) << "."
				          << ((ip >> 12) & 0xff) << ":"
				          << server.getClientPort(client)
				          << ": " << data << std::endl;
				std::cout << "hgserver: "
						<< server.sendData("MESSAGE RECEIVED", client)
						<< "B reply sent" << std::endl;
			}
		}
#endif
		SDL_Delay(20);
	}
	return EXIT_SUCCESS;
}
