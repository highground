// client.c++
// client class implementation file

#include <iostream>
#include <SDL/SDL_net.h>
#include <assert.h>

#include "client.h"
#include "msg.h"

Client::Client() {
	isConnected_ = 0;
	socketSet_ = NULL;
	serverSocket_ = NULL;
	messageBuffer_ = NULL;
	messageBufferLen_ = 0;
}

Client::~Client() {
	disconnect();
	if (messageBuffer_ != NULL)
		free(messageBuffer_);
}

unsigned int Client::sendData(const void* data, unsigned int bytes) {
	if (isConnected_ == 0)
		return 0;
	unsigned int sent = SDLNet_TCP_Send(serverSocket_, data, bytes);
	if (sent == bytes) {
		return sent;
	} else {
		disconnect();	
		return -1;
	}
}

// Watch it, this function returns -1 on epic fail
unsigned int Client::receiveData(void* data, unsigned int maxLength,
                         unsigned int timeout) {
	assert(data != NULL);
	if (isConnected_ == 0)
		return 0;
	signed int nReady = SDLNet_CheckSockets(socketSet_, timeout);
	if (nReady == -1) {
		std::cout << "ERROR: SDLNet_CheckSockets: " << SDLNet_GetError()
		          << std::endl;
		perror("SDLNet_CheckSockets");
		exit(1);
	}
	if (nReady == 0) {
		return 0;
	} else {
		if (SDLNet_SocketReady(serverSocket_) != 0) {
			signed int dataReceived = SDLNet_TCP_Recv(serverSocket_, data,
			                                          maxLength);
			if (dataReceived < 0) {
				std::cout << "ERROR: SDLNet_TCP_Recv: " << SDLNet_GetError()
				          << std::endl;
				disconnect();
				return -1;
			}
			if (dataReceived != 0) {
				return dataReceived;
			} else {
				std::cout << "You have been disconnected from the server."
				          << std::endl;
				disconnect();
				return -1;
			}
		}
	}
	// This shouldn't happen.
	return 0;
}

bool Client::connect(const char* hostName, unsigned int port) {
	IPaddress ip;
	isConnected_ = 1;
	if (SDLNet_ResolveHost(&ip, hostName, port) == -1) {
		std::cout << "ERROR: SDLNet_ResolveHost: " << SDLNet_GetError()
		          << std::endl;
		isConnected_ = 0;
	}
	serverSocket_ = SDLNet_TCP_Open(&ip);
	if (serverSocket_ == NULL) {
		std::cout << "ERROR: SDLNet_TCP_Open: " << SDLNet_GetError()
		          << std::endl;
		isConnected_ = 0;
	}
	socketSet_ = SDLNet_AllocSocketSet(1);
	if (socketSet_ == NULL) {
		std::cout << "ERROR: SDLNet_AllocSocketSet: " << SDLNet_GetError()
		          << std::endl;
		SDLNet_TCP_Close(serverSocket_);
		isConnected_ = 0;
	}
	if (SDLNet_TCP_AddSocket(socketSet_, serverSocket_) == -1) {
		std::cout << "ERROR: SDLNet_TCP_AddSocket: " << SDLNet_GetError()
		          << std::endl;
		SDLNet_TCP_Close(serverSocket_);
		SDLNet_FreeSocketSet(socketSet_);
		isConnected_ = 0;
	}
	return isConnected_;
}

void Client::disconnect() {
	if (isConnected_ == 0)
		return;
	std::cout << "Disconnecting from server.\n";
	if (SDLNet_TCP_DelSocket(socketSet_, serverSocket_) == -1)
		std::cout<<"ERROR: SDLNet_TCP_DelSocket: " << SDLNet_GetError()
				 << std::endl;
	SDLNet_TCP_Close(serverSocket_);
	SDLNet_FreeSocketSet(socketSet_);
	socketSet_ = NULL;
	serverSocket_ = NULL;
	isConnected_ = 0;
	if (messageBuffer_ != NULL)
		free(messageBuffer_);
	messageBuffer_ = NULL;
	messageBufferLen_ = 0;
}

#define BUFFER_SIZE 256

// This should be a latency-proof method for receiving messages from clients.
void Client::getMessages() {
	unsigned int received;
	if (isConnected_ == 0) 
		return;
	if (messageBuffer_ == NULL) {
		messageBuffer_ = (char*) malloc(BUFFER_SIZE);
		if (messageBuffer_ == NULL) {
			std::cout << "memory allocation failed.\n";
			exit(1);
		}
	}
	for (;; ++messageBufferLen_) {
		// Check to make sure the message won't overflow the buffer.
		// This will be handled better in the future.
		if (messageBufferLen_ >= BUFFER_SIZE) {
			std::cout<<"Message too long.\n";
			disconnect();
			return;
		}
		received = receiveData(&(messageBuffer_[messageBufferLen_]), 1, 1);

		// If no data was received, break, the current buffer is kept
		// so that it can continue to be filled again later.
		if (received == 0) {
				break;
		}
		// Connection error.
		if (received == (unsigned int)-1) {
				break;
		}
		// Is it the end of the message?
		if (messageBuffer_[messageBufferLen_] == '\0') {
			//std::cout << messageBuffer_ << "\n";
			parseMessage(messageBuffer_, BUFFER_SIZE, 0);
			free(messageBuffer_);
			messageBuffer_ = NULL;
			messageBufferLen_ = 0;
			break;
		}
	}
}
