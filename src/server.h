// server.h
// server class header file

#ifndef SERVER_H
#define SERVER_H

#include <SDL/SDL_net.h>

class Server {
	public:
		Server(unsigned int, unsigned int);
		~Server();
		
		unsigned int getNClients();
		bool addClient();
		void removeClient(unsigned int);
		unsigned int getClientIp(unsigned int);
		unsigned int getClientPort(unsigned int);
		
		unsigned int getNActiveSockets(unsigned int);
		unsigned int receiveData(void*, unsigned int, unsigned int);
		unsigned int sendData(const void*, unsigned int, unsigned int);

		void getMessages();
		
	private:
		TCPsocket serverSocket_;
		
		unsigned int nClients_;
		TCPsocket* clientSockets_;
		IPaddress* clientIps_;
		SDLNet_SocketSet socketSet_;

		char** clientBuffers_;
		unsigned int* clientMessageLens_;
};

#endif // SERVER_H
