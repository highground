// textbox.h
// textbox class header file

#ifndef TEXTBOX_H
#define TEXTBOX_H

#include "sfont.h"

class TextBox {
	public:
		TextBox();
		TextBox(unsigned int);
		~TextBox();
		
		unsigned int getX();
		void setX(unsigned int);
		unsigned int getY();
		void setY(unsigned int);
		unsigned int getNLines();
		void setNLines(unsigned int);
		
		SFont_Font* getFont();
		void setFont(SFont_Font*);
		
		void write(const char*);
		void displayText();
		
	private:
		unsigned int x_;
		unsigned int y_;
		
		unsigned int nLines_;
		char** line_;
		
		SFont_Font* font_;
		
};

#endif
