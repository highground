// spells.c++
// spell utility functions implementation file

#include "highground.h"
#include "map.h"
#include "object.h"

extern Map* map;

void buildWall(unsigned int startX, unsigned int startY, unsigned int length,
               unsigned char direction, signed int height) {
	signed int newHeight;
	if (direction == NORTH) {
		for (unsigned int i = 0; i < length; ++i) {
			if ((signed int)(startY - i) >= 0) {
				newHeight = (signed int)(map->getTile(0, startX, startY - i) +
				                         height);
				if (newHeight < 0) {
					map->setTile(0, startX, startY - i, 0);
				} else if (newHeight > 255) {
					map->setTile(0, startX, startY - i, 255);
				} else {
					map->setTile(0, startX, startY - i, newHeight);
				}
			}
		}
	} else if (direction == EAST) {
		for (unsigned int i = 0; i < length; ++i) {
			if ((signed int)(startX + i) <= (signed int)map->getWidth() - 1) {
				newHeight = (signed int)(map->getTile(0, startX + i, startY) +
				                         height);
				if (newHeight < 0) {
					map->setTile(0, startX + i, startY, 0);
				} else if (newHeight > 255) {
					map->setTile(0, startX + i, startY, 255);
				} else {
					map->setTile(0, startX + i, startY, newHeight);
				}
			}
		}
	} else if (direction == SOUTH) {
		for (unsigned int i = 0; i < length; ++i) {
			if ((signed int)(startY + i) <= (signed int)map->getHeight() - 1) {
				newHeight = (signed int)(map->getTile(0, startX, startY + i) +
				                         height);
				if (newHeight < 0) {
					map->setTile(0, startX, startY + i, 0);
				} else if (newHeight > 255) {
					map->setTile(0, startX, startY + i, 255);
				} else {
					map->setTile(0, startX, startY + i, newHeight);
				}
			}
		}
	} else if (direction == WEST) {
		for (unsigned int i = 0; i < length; ++i) {
			if ((signed int)(startX - i) >= 0) {
				newHeight = (signed int)(map->getTile(0, startX - i, startY) +
				                         height);
				if (newHeight < 0) {
					map->setTile(0, startX - i, startY, 0);
				} else if (newHeight > 255) {
					map->setTile(0, startX - i, startY, 255);
				} else {
					map->setTile(0, startX - i, startY, newHeight);
				}
			}
		}
	}
}

void buildMountain(unsigned int centreX, unsigned int centreY,
                   unsigned int radius, signed int height) {
	unsigned int radSq = radius * radius;
	unsigned int currentRadSq;
	signed int newHeight;
	for (unsigned int y = 0; y < map->getHeight(); ++y) {
		for (unsigned int x = 0; x < map->getWidth(); ++x) {
			currentRadSq = ((x - centreX) * (x - centreX)) +
			               ((y - centreY) * (y - centreY));
			if (currentRadSq < radSq) {
				newHeight = map->getTile(0, x, y) +
				            (height * (signed int)(radSq - currentRadSq) /
				             (signed int)radSq);
				if (newHeight < 0) {
					map->setTile(0, x, y, 0);
				} else if (newHeight > 255) {
					map->setTile(0, x, y, 255);
				} else {
					map->setTile(0, x, y, newHeight);
				}
			}
		}
	}
}
