#include <iostream>
#include <SDL/SDL.h>
#include <assert.h>

#include "highground.h"
#include "gfx.h"
#include "sfont.h"
#include "map.h"
#include "object.h"
#include "fountain.h"
#include "wizard.h"
#include "spells.h"
#include "textbox.h"

extern SDL_Surface* screen;
SFont_Font* Font;
Map* map;
TextBox* messages;

Object **objects = NULL;
unsigned int nObjects = 0;

unsigned int xRes = 800;
unsigned int yRes = 600;
unsigned int bpp = 32;
unsigned int windowX = 0;
unsigned int windowY = 0;
unsigned int windowW = 25;
unsigned int windowH = 18;

bool inputLocked = false;
Uint8* keys;
Uint8 mouseButtons;
signed int mouseX, mouseY;

extern unsigned char screenFilterAlpha;
extern SDL_Surface* screenFilter;

int main(int argc, char *argv[]);
void drawObjects();
void drawScreen();
void lockInput();
void unlockInput();

int main(int argc, char *argv[]) {
	srand(time(NULL));
	
	initSDL(xRes, yRes, bpp);	
	initFonts();
	
	map = new Map();
	map->setSize(128, 128);
	map->initialise();
	
	map->subdivideDisplace(100, 0.7, 6);
	*getObjectPointer() = new Wizard(8, 8);
	
	*getObjectPointer() = new Fountain(0, 0);
	*getObjectPointer() = new Fountain(map->getWidth() - 1, 0);
	*getObjectPointer() = new Fountain(0, map->getHeight() - 1);
	
	messages = new TextBox(2);
	messages->setFont(Font);
	messages->write("Highground Started");
	messages->write("Try to reach the bottom right hand corner of the map.");
	
	SDL_Event event;
	
	unsigned int frameTicks = 0;
	unsigned int objectTicks = 0;
	unsigned int scrollTicks = 0;
	
	lockInput();
	
	while (1) {
		// Every 25ms, call tick() on all the objects.
		if (SDL_GetTicks() - objectTicks > 25) {
			for (unsigned int i = 0; i < nObjects; i++) {
				if (objects[i] != NULL)
					objects[i]->tick();
			}
			objectTicks = SDL_GetTicks();
		}
		
		// Every 25ms, redraw the screen.
		if (SDL_GetTicks() - frameTicks > 25) {
			map->drawEarth();
			drawObjects();
			map->drawWater();
			drawScreen();
			frameTicks = SDL_GetTicks();
		}
		
		SDL_PollEvent(&event);
		if (event.type == SDL_QUIT) {
			exit(0);
		}
		keys = SDL_GetKeyState(NULL);
		if (keys[SDLK_ESCAPE] != 0) {
			exit(0);
		}
		// Moving the wizard with the arrow keys is temporary.
		if (keys[SDLK_UP])
				objects[0]->move(NORTH);
		if (keys[SDLK_DOWN])
				objects[0]->move(SOUTH);
		if (keys[SDLK_RIGHT])
				objects[0]->move(EAST);
		if (keys[SDLK_LEFT])
				objects[0]->move(WEST);
		
		// Save mouse buttons state and cursor position
		mouseButtons = SDL_GetMouseState(&mouseX, &mouseY);
		// If the middle mouse button has been pressed, unlock the mouse
		// and keyboard.
		if (mouseButtons & SDL_BUTTON(2)) 
			unlockInput();
		// If any other mouse button has been prssed, lock the mouse
		// and keyboard.
		else if ((mouseButtons & SDL_BUTTON(1)) || (mouseButtons & SDL_BUTTON(3)))
			lockInput();
		
		// Every 50ms, check if the mouse as at the edge of the screen and
		// if so, scroll the map.
		if (SDL_GetTicks() - scrollTicks > 50) {
			if ((xRes - mouseX < 10) && (windowX + windowW < map->getWidth()))
				++windowX;
			if ((mouseX < 10) && (windowX > 0))
				--windowX;
			if ((yRes - mouseY < 10) && (windowY + windowH < map->getHeight()))
				++windowY;
			if ((mouseY < 10) && (windowY > 0))
				--windowY;
			scrollTicks = SDL_GetTicks();
		}
		
		SDL_Delay(20);
	}
	
	return EXIT_SUCCESS;
}

void drawObjects() {
	unsigned int i;
	for (i = 0; i < nObjects; i++) {
		if (objects[i] != NULL) {
			objects[i]->draw();
		}
	}
}

void drawScreen() {
	if (screenFilter != NULL) {
		drawSurface(screenFilter, 0, 0, screenFilterAlpha);
	}
	messages->displayText();
	// draw a minimap
	for (unsigned int y = 0; y < map->getHeight(); ++y) {
		for (unsigned int x = 0; x < map->getWidth(); ++x) {
			if (map->getTile(1, x, y) == 0) {
				putPixel(screen, x, (SPRITE_HEIGHT * windowH) -
				         map->getHeight() + y, map->getTile(0, x, y) << 8);
			} else {
				putPixel(screen, x, (SPRITE_HEIGHT * windowH) -
				         map->getHeight() + y, 0x3f3fff);
			}
		}
	}
	// draw a red square for the wizard (TEMPORARY)
	fillRect(screen, objects[0]->getX() - 1, (SPRITE_HEIGHT * windowH) -
	         map->getHeight() + objects[0]->getY() - 1, 3, 3, 0xff0000);
	// draw a border to show where the screen is looking
	fillRect(screen, windowX, (SPRITE_HEIGHT * windowH) - map->getHeight() +
	         windowY, windowW, 1, 0xff0000); // top
	fillRect(screen, windowX + windowW - 1, (SPRITE_HEIGHT * windowH) -
	         map->getHeight() + windowY + 1, 1, windowH - 1, 0xff0000); // right
	fillRect(screen, windowX, (SPRITE_HEIGHT * windowH) - map->getHeight() +
	         windowY + windowH - 1, windowW - 1, 1, 0xff0000); // bottom
	fillRect(screen, windowX, (SPRITE_HEIGHT * windowH) - map->getHeight() +
	         windowY + 1, 1, windowH - 2, 0xff0000); // left
	SDL_UpdateRect(screen, 0, 0, 0, 0);
}

// This function will lock the mouse and keystrokes.
void lockInput() {
	if (inputLocked == true) {
			return;
	} else if (SDL_WM_GrabInput(SDL_GRAB_ON) != SDL_GRAB_ON) {
		std::cout << "Could not grab mouse and keyboard input..." << std::endl;
		inputLocked = false;
	} else {
		inputLocked = true;
	}
}

// This function will unlock the mouse and keystrokes.
void unlockInput() {
	if (inputLocked == false) {
			return;
	} else if (SDL_WM_GrabInput(SDL_GRAB_OFF) != SDL_GRAB_OFF) {
		// This should really never happen...
		std::cout << "Could not ungrab mouse and keyboard input..."
		          << std::endl;
		inputLocked = true;
	} else {
		inputLocked = false;
	}
}
