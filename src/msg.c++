// msg.c++
// This file will deal with all message parsing, handling and sending
// This file is a mess.. can you think of a neat way of doing this?

#include <iostream>
#include <assert.h>
#ifdef SERVER
	#include "server.h"
#endif 
#ifdef CLIENT
	#include "client.h"
#endif
#include "msg.h"
#include "map.h"

#ifdef CLIENT
	extern Client* client;
#endif
#ifdef SERVER
	extern Server* server;
#endif

extern Map* map;

static void invalidMessage(const char*, const char*, unsigned int);

// Message handler prototypes here ===================================
static void getMap(const char** tokens, const unsigned int nTokens, 
		const char* message, const unsigned int sender);

// Message sender prototypes go in the header!!

// Utility functions here ============================================
// This message parser splits the message up into tokens as messages have
// '|' as a seperator. The relevant message handler is then called and is
// passed the tokenised message as an argument.
void parseMessage(char* message, unsigned int bufferSize, 
				  unsigned int sender) {
	unsigned int nTokens = 0;
	char** tokens = NULL;

	tokens = (char**) realloc(tokens, sizeof(char*) * (nTokens + 1));
	tokens[nTokens] = strtok (message,"|");
	++nTokens;

	while (1) {
		tokens = (char**) realloc(tokens, sizeof(char*) * (nTokens + 1));
		tokens[nTokens] = strtok (NULL, "|");
		if (tokens[nTokens] == NULL)
			break;
		++nTokens;
	}

	// Messages will be dealt with in a nice way in the future.
	// Received message to send the map
	if (tokens[0] != NULL) {
		if (strcmp(tokens[0], "MAPSEND") == 0) {
			getMap((const char**)tokens, nTokens, message, sender);
		}
	} else {
		invalidMessage(message, "No message handler", sender);
	}
	free(tokens);
}

#ifdef SERVER
static bool sendMessage(const char* message, unsigned int client) {
	std::cout << "Sending:" << message << "\n";
	if (server->sendData(message, strlen(message) + 1, client) <= 0) {
		std::cout << "Map message sending to " << client << " failed.\n";
		return false;
	}
	return true;
}
#endif
#ifdef CLIENT
static bool sendMessage(const char* message) {
	std::cout << "Sending:" << message << "\n";
	if (client->sendData(message, strlen(message) + 1) <= 0) {
		std::cout << "Map message sending to " << client << " failed.\n";
		return false;
	}
	return true;
}
#endif

// A utility function for dealing with an invalid message.
static void invalidMessage(const char* message, const char* reason, 
		unsigned int sender) {
	std::cout << "Invalid message: " << message << "\n" 
		<< reason << "\nsent by: " << sender <<"\n";
#ifdef CLIENT
	client->disconnect();
#endif
#ifdef SERVER
	server->removeClient(sender);
#endif
}

// Message handler bodies here =======================================
static void getMap(const char** tokens, const unsigned int nTokens, 
		const char* message, const unsigned int sender) {
#ifdef CLIENT
	unsigned int width = 0;
	unsigned int height = 0;
	unsigned int totalBytesReceived = 0;
	unsigned int bytes;
	unsigned int totalBytes;
	unsigned char tile;
	// Check for correct number of parameters
	if (nTokens != 3) {
		invalidMessage(message, "Invalid parameter count", sender);
		return;
	}
	width = atoi(tokens[1]);	
	height = atoi(tokens[2]);
	totalBytes = width * height;
	// Sanity check parameters
	if (totalBytes > 65536) {
		invalidMessage(message, "Map too large", sender);
		return;
	}
	std::cout << "Downloading map. Dimensions: " << width << " x " <<
		height << "\n";
	map->uninitialise();
	map->setSize(width, height);
	map->initialise();

	while (totalBytesReceived < totalBytes) {
		bytes = client->receiveData(&tile, 1, 1000);
		if (bytes == (unsigned int)-1) {
			std::cout << "Map download failed.\n";
			client->disconnect();
			return;
		}
		else
			map->setTile(0, totalBytesReceived % width, 
					totalBytesReceived / width, tile);
			totalBytesReceived += bytes;
		if (totalBytesReceived % (totalBytes / 10) == 1)
			std::cout << "\r" << totalBytesReceived << " bytes received. [" 
				<< (totalBytesReceived / totalBytes) * 100 << "]%";
	}
	std::cout << "\r" << totalBytesReceived<< " bytes received. [" 
		<< (totalBytesReceived / totalBytes) * 100 << "%]\n";

	std::cout << "Map has been fully received.\n";
#endif	
}

// Message sender bodies here ========================================
void sendMap(unsigned int client) {
#ifdef SERVER 
	unsigned int bytesSent = 0;
	unsigned int width = map->getWidth();
	unsigned int height = map->getHeight();
	const unsigned int totalBytes = width * height;
	unsigned char byte;
	char message[64];
	sprintf(message, "MAPSEND|%u|%u", width, height);
	sendMessage(message, client);
	std::cout << "Sending map. Dimensions: " << width << " x " <<
		height << "\n";
	while (bytesSent < totalBytes) {
		byte = map->getTile(0, bytesSent % width, bytesSent / width);
		if (server->sendData(&byte, 1, client) <= 0) {
			std::cout << "Map sending to " << client << " failed. \n";
			return;
		}
		++bytesSent;
		if (bytesSent % (totalBytes / 100) == 1)
			std::cout << "\r" << bytesSent << " bytes sent. [" 
				<< (bytesSent / totalBytes) * 100 << "%]";
	}
	std::cout << "\r" << bytesSent << " bytes sent. [" 
		<< (bytesSent / totalBytes) * 100 << "%]\n";
	std::cout << "Map has been fully sent.\n";
#endif
}




