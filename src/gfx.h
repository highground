#ifndef GFX_H 
#define GFX_H 

void initSDL(unsigned int, unsigned int, unsigned int); 

void sLock(SDL_Surface*);

void sUlock(SDL_Surface*);

void drawSurface(SDL_Surface*, unsigned int, unsigned int, unsigned int,
                 unsigned int, unsigned int, unsigned int);

void drawSurface(SDL_Surface*, unsigned int, unsigned int);

void drawSurface(SDL_Surface*, unsigned int, unsigned int, unsigned char);

void drawToTile(SDL_Surface* surface, unsigned int x, unsigned int y, 
              unsigned char alpha);

void drawToTile(SDL_Surface* surface, unsigned int x, unsigned int y);

SDL_Surface* loadBitmap(const char*);

Uint32 getPixel(SDL_Surface*, unsigned int, unsigned int);

void putPixel(SDL_Surface*, unsigned int, unsigned int, Uint32);

void fillRect(SDL_Surface*, unsigned int, unsigned int, unsigned int,
              unsigned int, unsigned int);

SDL_Surface* tint (signed char, signed char, signed char, SDL_Surface*); 

void untintScreen();

SDL_Surface* tint (SDL_Surface*, signed char, signed char, signed char);

void tintTile(unsigned int, unsigned int);

#endif // GFX_H
