// object.h
// object classes header file

#ifndef OBJECT_H
#define OBJECT_H

#include <SDL/SDL.h>

#include "map.h"
#include "highground.h"

class Object {
	public:
		Object();
		Object(unsigned int, unsigned int);
		
		unsigned int getX();
		unsigned int getY();
		
		virtual void draw() = 0;
		virtual void activate() = 0;
		virtual void tick() = 0;
		virtual void move(unsigned char);
		
	protected:
		unsigned int x_;
		unsigned int y_;
		
};

Object** getObjectPointer();
void freeObjects();

#endif // OBJECT_H
