// highground.h
// i'm not sure what the purpose of this file is

#ifndef HIGHGROUND_H
#define HIGHGROUND_H

#define SPRITE_WIDTH 32
#define SPRITE_HEIGHT 32

#define MAP_LAYERS 2
// 0 for ground height
// 1 for water-fountain relations

#define NORTH 0
#define EAST 1
#define SOUTH 2
#define WEST 3
#define NO_DIRECTION 4

#endif // HIGHGROUND_H
