// map.h
// map class header file

#ifndef MAP_H
#define MAP_H

#include <SDL/SDL.h>

class Map {
	public:
		Map();
		~Map();

		void setSize(unsigned int, unsigned int);
		
		void initialise();
		void uninitialise();
		
		bool load(char*);
		bool save(char*);
		
		unsigned int getWidth();
		unsigned int getHeight();
		
		unsigned char getTile(unsigned int, unsigned int, unsigned int);
		void setTile(unsigned int, unsigned int, unsigned int, unsigned char);
		
		void drawEarth();
		void drawEarth(unsigned int, unsigned int);
		void drawWater();
		void drawWater(unsigned int, unsigned int);
		
		void subdivideDisplace(unsigned int, float, unsigned int);
		void offsetArea(const float, const float, const float, const float);
		
		unsigned char*** getMapData();

	private:
		bool initialised_;
		
		unsigned int width_;
		unsigned int height_;
		
		unsigned char ***map_;
		
		SDL_Surface* whiteMask_;
		SDL_Surface* grass_;
		SDL_Surface* water_;
};

#endif // MAP_H
