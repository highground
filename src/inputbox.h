// inputbox.h
// inputbox class header

#ifndef TEXTBOX_H
#define TEXTBOX_H

class InputBox {
	public:
		InputBox();
		~InputBox();

		void update();
		void draw();

		void setText(const char*);
		char* getText();
		bool isFinished();
		void setActive();
		bool isActive();

	private:
		char* text_;
		unsigned char len_;
		bool isActive_;
		bool isFinished_;

		static unsigned int nInputBoxes_;
};

#endif //TEXTBOX_H
