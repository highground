// object.c++
// object classes implementation file

#include <assert.h>

#include "highground.h"
#include "object.h"
#include "map.h"
#ifdef CLIENT
#include "gfx.h"
#endif //CLIENT

extern Map* map;
extern Object **objects;
extern unsigned int nObjects;

Object::Object() {
	assert(0);
}

Object::Object(unsigned int x, unsigned int y) {
	x_ = x;
	y_ = y;
}

unsigned int Object::getX() {
	return x_;
}

unsigned int Object::getY() {
	return y_;
}

void Object::move(unsigned char direction) {
	assert(0);
}

// This function will (if necessary) allocate a new Object pointer, and
// return the index in the Object pointer array of where it resides. 
Object** getObjectPointer() {
	for (unsigned int i = 0; i < nObjects; ++i) {
		if (objects[i] == NULL) {
			return &objects[i];
		}
	}
	++nObjects;
	objects = (Object**)realloc(objects, nObjects * sizeof(Object*));
	if (objects == NULL) {
			perror("realloc");
	}
	return &objects[nObjects - 1];
}

// this doesn't seem to work properly.
void freeObjects() {
	for (unsigned int i = 0; i < nObjects; ++i) {
		if (objects[i] != NULL) {
			delete objects[i];
		}
	}
	free(objects);
}

