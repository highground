// textbox.c++
// textbox class implementation file

#include <iostream>
#include <SDL/SDL.h>

#include "textbox.h"
#include "sfont.h"

extern SDL_Surface *screen;

extern SFont_Font* Font;

TextBox::TextBox() {
	x_ = 0;
	y_ = 0;
	nLines_ = 1;
	line_ = new char*;
	line_[0] = NULL;
	font_ = NULL;
}

TextBox::TextBox(unsigned int nLines) {
	x_ = 0;
	y_ = 0;
	nLines_ = nLines;
	line_ = new char* [nLines];
	for (unsigned int i = 0; i < nLines; ++i) {
		line_[i] = NULL;
	}
	font_ = NULL;
}

TextBox::~TextBox() {
	for (unsigned int i = 0; i < nLines_; ++i) {
		delete [] line_[i];
	}
	delete [] line_;
}

unsigned int TextBox::getX() {
	return x_;
}

void TextBox::setX(unsigned int x) {
	x_ = x;
}

unsigned int TextBox::getY() {
	return y_;
}

void TextBox::setY(unsigned int y) {
	y_ = y;
}

unsigned int TextBox::getNLines() {
	return nLines_;
}

void TextBox::setNLines(unsigned int nLines) {
	nLines_ = nLines;
	// line_ array needs updating
	// DO NOT USE THIS FUNCTION YET
}

SFont_Font* TextBox::getFont() {
	return font_;
}

void TextBox::setFont(SFont_Font* font) {
	font_ = font;
}

void TextBox::write(const char* text) {
	delete [] line_[0];
	for (unsigned int i = 0; i < nLines_ - 1; --i) {
		line_[i] = line_[i + 1];
	}
	line_[nLines_ - 1] = new char [strlen(text) + 1];
	strcpy(line_[nLines_ - 1], text);
}

void TextBox::displayText() {
	for (unsigned int i = 0; i < nLines_; ++i) {
		SFont_Write(screen, font_, x_, y_ + (i * SFont_TextHeight(font_)),
		            line_[i]);
	}
}
