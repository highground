// fountain.c++
// fountain class implementation file

#include <assert.h>

#include "map.h"
#include "object.h"
#include "fountain.h"

unsigned char Fountain::nFountains_ = 0;

extern Map* map;

Fountain::Fountain() {
	assert(0);
}

Fountain::Fountain(unsigned int x, unsigned int y) : Object(x, y) {
	assert(nFountains_ < 8);
	flowRate_ = 1;
	fountainNo_ = nFountains_;
	map->setTile(1, x, y, map->getTile(1, x, y) | (1 << fountainNo_));
	++nFountains_;
}

void Fountain::draw() {
	
}

void Fountain::activate() {
	
}

void Fountain::tick() {
	assert(x_ < map->getWidth());
	assert(y_ < map->getHeight());
	// AAAAAAAAAAAAAAAAAAAAAAAAAARRRRRRRRRRGGGGGGGHHHHHHHHHHHHHHH!!!!!!!!!!!!!!
	// create list of tiles to flood
	unsigned int* lowX = new unsigned int [flowRate_];
	unsigned int* lowY = new unsigned int [flowRate_];
	for (unsigned int i = 0; i < flowRate_; ++i) {
		lowX[i] = x_;
		lowY[i] = y_;
	}
	bool isNextToWater;
	for (unsigned int y = 0; y < map->getHeight(); ++y) {
		for (unsigned int x = 0; x < map->getWidth(); ++x) {
			// only check tiles not in bodies of water being fed from the
			// current fountain
			if ((unsigned char)(map->getTile(1, x, y) &
			                    (1 << fountainNo_)) == 0) {
				// check if the tile is adjacent to a body of water being fed
				// from the current fountain
				isNextToWater = 0;
				// check if a tile to the north exists
				if (y > 0) {
					if ((unsigned char)(map->getTile(1, x, y - 1) &
					                    (1 << fountainNo_)) != 0) {
						isNextToWater = 1;
					}
				}
				// check if a tile to the east exists
				if (x < map->getWidth() - 1 && isNextToWater == 0) {
					if ((unsigned char)(map->getTile(1, x + 1, y) &
					                    (1 << fountainNo_)) != 0) {
						isNextToWater = 1;
					}
				}
				// check if a tile to the south exists
				if (y < map->getHeight() - 1 && isNextToWater == 0) {
					if ((unsigned char)(map->getTile(1, x, y + 1) &
					                    (1 << fountainNo_)) != 0) {
						isNextToWater = 1;
					}
				}
				// check if a tile to the west exists
				if (x > 0 && isNextToWater == 0) { // w
					if ((unsigned char)(map->getTile(1, x - 1, y) &
					                    (1 << fountainNo_)) != 0) {
						isNextToWater = 1;
					}
				}
				if (isNextToWater == 1) {
					// we are currently looking at a tile that is not part of a
					// body of water being fed from the current fountain, but
					// is adjacent to a tile that is part of a body of water
					// being fed from the current fountain; thus it is an
					// eligible target for flooding
					// insert it into the list of lowest such tiles if it is
					// low enough
					for (unsigned int i = 0; i < flowRate_; ++i) {
						if (map->getTile(0, x, y) <
						    map->getTile(0, lowX[i], lowY[i]) ||
							(lowX[i] == x_ && lowY[i] == y_)) {
							for (unsigned int j = flowRate_ - 1; j > i; --j) {
								lowX[j] = lowX[j - 1];
								lowY[j] = lowY[j - 1];
							}
							lowX[i] = x;
							lowY[i] = y;
						}
						break;
					}
				}
			}
			
		}
	}
	assert(x_ < map->getWidth());
	assert(y_ < map->getHeight());
	// flood the tiles contained in the list
	for (unsigned int i = 0; i < flowRate_; ++i) {
		if (map->getTile(1, lowX[i], lowY[i]) == 0) {
			// if the tile wasn't part of another body of water, flood it
			// normally and mark it as being fed from every fountain that feeds
			// the current fountain's body of water
			map->setTile(1, lowX[i], lowY[i], map->getTile(1, x_, y_));
		} else {
			// if the tile was part of another body of water, find every other
			// tile in the same body of water and mark it as being fed from
			// every fountain that feeds the current fountain's body of water
			for (unsigned int y = 0; y < map->getHeight(); ++y) {
				for (unsigned int x = 0; x < map->getWidth(); ++x) {
					if ((unsigned char)(map->getTile(1, x, y) &
					                    map->getTile(1, lowX[i], lowY[i]))
					    != 0) {
						map->setTile(1, x, y,
									 (unsigned char)(map->getTile(1, x, y) |
													 map->getTile(1, x_, y_)));
					}
				}
			}
		}
	}
	assert(x_ < map->getWidth());
	assert(y_ < map->getHeight());
	// free memory
	delete [] lowX;
	delete [] lowY;
}

unsigned int Fountain::getFlowRate() {
	return flowRate_;
}

void Fountain::setFlowRate(unsigned int flowRate) {
	flowRate_ = flowRate;
}
