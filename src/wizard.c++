// wizard.c++ 
// wizard class function bodies

#include <SDL/SDL.h>
#include <iostream>
#include <assert.h>

#include "highground.h"
#include "map.h"
#include "wizard.h"

#ifdef CLIENT
#include "gfx.h"
#include "textbox.h"

extern TextBox* messages;
#endif //CLIENT

extern Map* map;

unsigned int Wizard::nWizards = 0;

signed char WizardColourR[]={ 127, -127, -127,   63, -127,   63, 0};
signed char WizardColourG[]={-127,  127, -127, -127,   63,   63, 0};
signed char WizardColourB[]={-127, -127,  127,   63,   63, -127, 0};

Wizard::Wizard() {
		assert(false);
}

Wizard::Wizard(unsigned int x, unsigned int y) : Object(x, y) {
	isAlive_ = 1;
	speed_ = 10000; // arbitrary default value
	moveDirection_ = NO_DIRECTION;
	faceDirection_ = SOUTH;
	loadImages();
	++nWizards;
}

Wizard::~Wizard() {
	unloadImages();
}

void Wizard::draw() {
	#ifdef CLIENT
	//drawSurface(south_, x_ * SPRITE_WIDTH, y_ * SPRITE_HEIGHT);	
	if (moveDirection_ == NO_DIRECTION) {
		if (faceDirection_ == NORTH) {
			drawToTile(north_, x_, y_);
		} else if (faceDirection_ == EAST) {
			drawToTile(east_, x_, y_);
		} else if (faceDirection_ == SOUTH) {
			drawToTile(south_, x_, y_);
		} else if (faceDirection_ == WEST) {
			drawToTile(west_, x_, y_);
		}
	} else if (moveDirection_ == NORTH) {
		drawToTile(walkNorth_[steps_], x_, y_ - 1);
	} else if (moveDirection_ == EAST) {
		drawToTile(walkEast_[steps_], x_, y_);
	} else if (moveDirection_ == SOUTH) {
		drawToTile(walkSouth_[steps_], x_, y_);
	} else if (moveDirection_ == WEST) {
		drawToTile(walkWest_[steps_], x_ - 1, y_);
	}
	#endif //CLIENT
}

void Wizard::activate() {
	
}

void Wizard::tick() {
	if (moveDirection_ != NO_DIRECTION) {
		++steps_;
		if (steps_ == 8) {
			if (moveDirection_ == NORTH) {
					--y_;
			} else if (moveDirection_ == EAST) {
					++x_;
			} else if (moveDirection_ == SOUTH) {
					++y_;
			} else if (moveDirection_ == WEST) {
					--x_;
			}
			moveDirection_ = NO_DIRECTION;
			if (x_ == map->getWidth() - 1 && y_ == map->getHeight() - 1 &&
				isAlive_ == 1) {
				#ifdef CLIENT
				messages->write("You have reached the end. Well done.");
				#endif //CLIENT
				isAlive_ = 0;
			}
			if (map->getTile(1, x_, y_) != 0 && isAlive_ == 1) {
				#ifdef CLIENT
				messages->write("You have drowned. How unfortunate.");
				#endif //CLIENT
				isAlive_ = 0;
			}
		}
	}
}

unsigned char** Wizard::findShortestPaths() {
	signed int dHeight;
	// create list of eastbound arc lengths
	// (tiles on the far east column don't need one as there is no tile to the
	// east of them)
	unsigned int** arcE = new unsigned int* [map->getWidth() - 1];
	for (unsigned int x = 0; x < map->getWidth() - 1; ++x) {
		arcE[x] = new unsigned int [map->getHeight()];
		for (unsigned int y = 0; y < map->getHeight(); ++y) {
			dHeight = (signed int)(map->getTile(0, x, y) -
			                       map->getTile(0, x + 1, y));
			arcE[x][y] = 1000 + (dHeight * dHeight); // arbitrary formula
		}
	}
	// create list of southbound arc lengths
	// (tiles on the far south row don't need one as there is no tile to the
	// south of them)
	unsigned int** arcS = new unsigned int* [map->getWidth()];
	for (unsigned int x = 0; x < map->getWidth(); ++x) {
		arcS[x] = new unsigned int [map->getHeight() - 1];
		for (unsigned int y = 0; y < map->getHeight() - 1; ++y) {
			dHeight = (signed int)(map->getTile(0, x, y) -
			                       map->getTile(0, x, y + 1));
			arcS[x][y] = 1000 + (dHeight * dHeight); // arbitrary formula
		}
	}
	// for each tile:
	// create a record of shortest distance, previous tile, and visited bool
	unsigned int** distance = new unsigned int* [map->getWidth()];
	unsigned char** previous = new unsigned char* [map->getWidth()];
	bool** visited = new bool* [map->getWidth()];
	for (unsigned int x = 0; x < map->getWidth(); ++x) {
		distance[x] = new unsigned int [map->getHeight()];
		previous[x] = new unsigned char [map->getHeight()];
		visited[x] = new bool [map->getHeight()];
		for (unsigned int y = 0; y < map->getHeight(); ++y) {
			distance[x][y] = 0xffffffff; // 0xffffffff represents infinity
			previous[x][y] = NO_DIRECTION; // no previous tile...
			visited[x][y] = 0; // ...as a route has not been established yet
		}
	}
	unsigned int curX = x_; // start on the...
	unsigned int curY = y_; // ...wizards's current tile
	distance[curX][curY] = 0; // set distance accordingly
	do {
		visited[curX][curY] = 1; // mark current tile as visited
		// if you can get to an adjacent tile quicker than the recorded
		// shortest distance for that tile, update that tile's shortest
		// distance and previous tile accordingly
		if (curY > 0) { // there is a tile to the north
			if (distance[curX][curY] + arcS[curX][curY - 1] <
			    distance[curX][curY - 1] ||
			    distance[curX][curY - 1] == 0xffffffff) {
				distance[curX][curY - 1] = distance[curX][curY] +
				                           arcS[curX][curY - 1];
				previous[curX][curY - 1] = SOUTH;
			}
		}
		if (curX < map->getWidth() - 1) { // there is a tile to the east
			if (distance[curX][curY] + arcE[curX][curY] <
				distance[curX + 1][curY] ||
				distance[curX + 1][curY] == 0xffffffff) {
				distance[curX + 1][curY] = distance[curX][curY] +
				                           arcE[curX][curY];
				previous[curX + 1][curY] = WEST;
			}
		}
		if (curY < map->getHeight() - 1) { // there is a tile to the south
			if (distance[curX][curY] + arcS[curX][curY] <
			    distance[curX][curY + 1] ||
				distance[curX][curY + 1] == 0xffffffff) {
				distance[curX][curY + 1] = distance[curX][curY] +
				                           arcS[curX][curY];
				previous[curX][curY + 1] = NORTH;
			}
		}
		if (curX > 0) { // there is a tile to the west
			if (distance[curX][curY] + arcE[curX - 1][curY] <
			    distance[curX - 1][curY] ||
				distance[curX - 1][curY] == 0xffffffff) {
				distance[curX - 1][curY] = distance[curX][curY] +
				                           arcE[curX - 1][curY];
				previous[curX - 1][curY] = EAST;
			}
		}
		// set the current tile to the closest unvisited tile within one turn's
		// range of the wizard
		for (unsigned int y = 0; y < map->getHeight(); ++y) {
			for (unsigned int x = 0; x < map->getWidth(); ++x) {
				if (visited[x][y] == 0 && distance[x][y] <= speed_) {
					if (distance[x][y] < distance[curX][curY] ||
					    visited[curX][curY] == 1) {
						curX = x;
						curY = y;
					}
				}
			}
		}
	// repeat while there are still unvisited tiles
	} while (visited[curX][curY] == 0);
	// free all unnecessary memory
	// eastbound arc lengths
	for (unsigned int x = 0; x < map->getWidth() - 1; ++x) {
		delete [] arcE[x];
	}
	delete [] arcE;
	// southbound arc lengths, shortest distances and visited bools
	for (unsigned int x = 0; x < map->getWidth(); ++x) {
		delete [] arcS[x];
		delete [] distance[x];
		delete [] visited[x];
	}
	delete [] distance;
	delete [] visited;
	return previous; // return the whole mess; it can be made sense of elsewhere
}

void Wizard::move(unsigned char moveDirection) {
	if (isAlive_ == 1) {
		if (moveDirection_ != NO_DIRECTION) {
				return;
		} else {
			if ((moveDirection == NORTH && y_ > 0) ||
			    (moveDirection == EAST && x_ < map->getWidth() - 1) ||
			    (moveDirection == SOUTH && y_ < map->getHeight() - 1) ||
			    (moveDirection == WEST && x_ > 0)) {
				moveDirection_ = moveDirection;
				faceDirection_ = moveDirection;
				steps_ = 0;
			}
		}
	}
}

void Wizard::loadImages() {
	#ifdef CLIENT
	char fn[255];
	south_ = loadBitmap("images/wiz_south.bmp");
	tint(south_, WizardColourR[nWizards], WizardColourG[nWizards],
					WizardColourB[nWizards]);
	north_ = loadBitmap("images/wiz_north.bmp");
	tint(north_, WizardColourR[nWizards], WizardColourG[nWizards],
					WizardColourB[nWizards]);
	west_ = loadBitmap("images/wiz_west.bmp");
	tint(west_, WizardColourR[nWizards], WizardColourG[nWizards],
					WizardColourB[nWizards]);
	east_ = loadBitmap("images/wiz_east.bmp");
	tint(east_, WizardColourR[nWizards], WizardColourG[nWizards],
					WizardColourB[nWizards]);
	
	for (unsigned int i = 0; i < 8; i++) {
		sprintf(fn, "images/wiz_walksouth%i.bmp", i);
		walkSouth_[i] = loadBitmap(fn);	
		tint(walkSouth_[i], WizardColourR[nWizards], WizardColourG[nWizards],
					WizardColourB[nWizards]);
	}
	for (unsigned int i = 0; i < 8; i++) {
		sprintf(fn, "images/wiz_walknorth%i.bmp", i);
		walkNorth_[i] = loadBitmap(fn);	
		tint(walkNorth_[i], WizardColourR[nWizards], WizardColourG[nWizards],
					WizardColourB[nWizards]);
	}
	for (unsigned int i = 0; i < 8; i++) {
		sprintf(fn, "images/wiz_walkwest%i.bmp", i);
		walkWest_[i] = loadBitmap(fn);	
		tint(walkWest_[i], WizardColourR[nWizards], WizardColourG[nWizards],
					WizardColourB[nWizards]);
	}
	for (unsigned int i = 0; i < 8; i++) {
		sprintf(fn, "images/wiz_walkeast%i.bmp", i);
		walkEast_[i] = loadBitmap(fn);	
		tint(walkEast_[i], WizardColourR[nWizards], WizardColourG[nWizards],
					WizardColourB[nWizards]);
	}
	#endif
}

void Wizard::unloadImages() {
	#ifdef CLIENT
	std::cout<<"Unloading wizard images..\n";
	SDL_FreeSurface(south_);
	SDL_FreeSurface(north_);
	SDL_FreeSurface(west_);
	SDL_FreeSurface(east_);
	for (unsigned int i = 0; i < 8; i++) {
		SDL_FreeSurface(walkSouth_[i]);	
	}
	for (unsigned int i = 0; i < 8; i++) {
		SDL_FreeSurface(walkNorth_[i]);	
	}
	for (unsigned int i = 0; i < 8; i++) {
		SDL_FreeSurface(walkWest_[i]);	
	}
	for (unsigned int i = 0; i < 8; i++) {
		SDL_FreeSurface(walkEast_[i]);	
	}
	#endif
}
