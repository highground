// spells.h
// spell utility functions header file

#ifndef SPELLS_H
#define SPELLS_H

#include "object.h"

void buildWall(unsigned int, unsigned int,
               unsigned int, unsigned char, signed int);

void buildMountain(unsigned int, unsigned int, unsigned int, signed int);

#endif
