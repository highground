// server.c++
// server class implementation file

#include <iostream>
#include <SDL/SDL_net.h>
#include <assert.h>

#include "server.h"
#include "msg.h"

Server::Server(unsigned int port, unsigned int maxClients) {
	IPaddress ip;
	if (SDLNet_ResolveHost(&ip, NULL, port) == -1) {
		std::cout << "ERROR: SDLNet_ResolveHost: " << SDLNet_GetError()
		          << std::endl;
		exit(1);
	}
	serverSocket_ = SDLNet_TCP_Open(&ip);
	if (serverSocket_ == NULL) {
		std::cout << "ERROR: SDLNet_TCP_Open: " << SDLNet_GetError()
		          << std::endl;
		exit(1);
	}
	nClients_ = 0;
	clientSockets_ = NULL;
	clientIps_ = NULL;
	socketSet_ = SDLNet_AllocSocketSet(maxClients);
	if (socketSet_ == NULL) {
		std::cout << "ERROR: SDLNet_AllocSocketSet: " << SDLNet_GetError()
		          << std::endl;
		exit(1);
	}
	clientBuffers_ = NULL;
	clientMessageLens_ = NULL;
}

Server::~Server() {
	while (nClients_ > 0) {
		removeClient(0);
	}
	SDLNet_TCP_Close(serverSocket_);
	SDLNet_FreeSocketSet(socketSet_);
}

unsigned int Server::getNClients() {
	return nClients_;
}

bool Server::addClient() {
	TCPsocket newClient = SDLNet_TCP_Accept(serverSocket_);
	if (newClient != NULL) {
		++nClients_;
		clientSockets_ = (TCPsocket*)realloc(clientSockets_,
		                                     sizeof(TCPsocket) * nClients_);
		clientSockets_[nClients_ - 1] = newClient;
		if (SDLNet_TCP_AddSocket(socketSet_, clientSockets_[nClients_ - 1]) ==
		    -1) {
			std::cout << "ERROR: SDLNet_TCP_AddSocket: " << SDLNet_GetError()
			          << std::endl;
			exit(1);
		}
		clientIps_ = (IPaddress*)realloc(clientIps_,
		                                 sizeof(IPaddress) * nClients_);
		clientIps_[nClients_ - 1] = *SDLNet_TCP_GetPeerAddress(newClient);
		clientBuffers_ = (char**)realloc(clientBuffers_, 
				sizeof(char*) * nClients_);
		clientBuffers_[nClients_ - 1] = NULL;
		clientMessageLens_ = (unsigned int*)realloc(clientMessageLens_, 
				sizeof(unsigned int) * nClients_);
		clientMessageLens_[nClients_ - 1] = 0;
		return 1;
	} 
		return 0;
}

void Server::removeClient(unsigned int clientNo) {
	if (SDLNet_TCP_DelSocket(socketSet_, clientSockets_[clientNo]) == -1) {
		std::cout << "ERROR: SDLNet_DelSocket: " << SDLNet_GetError()
		          << std::endl;
		exit(1);
	}
	if (clientBuffers_[clientNo] != NULL)
		free(clientBuffers_[clientNo]);

	unsigned int ip = clientIps_[clientNo].host;
	std::cout << "hgserver: disconnecting client: "
				<< ((ip >> 0) & 0xff) << "."
				<< ((ip >> 4) & 0xff) << "."
				<< ((ip >> 8) & 0xff) << "."
				<< ((ip >> 12) & 0xff) << ":"
				<< clientIps_[clientNo].port << std::endl;
	SDLNet_TCP_Close(clientSockets_[clientNo]);

	// We can use memmove() instead here sometime.
	for (unsigned int i = clientNo; i < nClients_ - 1; ++i) {
		clientSockets_[i] = clientSockets_[i + 1];
		clientIps_[i] = clientIps_[i + 1];
		clientBuffers_[i] = clientBuffers_[i + 1];
		clientMessageLens_[i] = clientMessageLens_[i + 1];
	}
	--nClients_;
	clientSockets_ = (TCPsocket*)realloc(clientSockets_,
	                                     sizeof(TCPsocket) * nClients_);
	clientIps_ = (IPaddress*)realloc(clientIps_,
	                                 sizeof(IPaddress) * nClients_);
	clientBuffers_ = (char**)realloc(clientBuffers_, 
									 sizeof(char*) * nClients_);
	clientMessageLens_ = (unsigned int*)realloc(clientMessageLens_, 
			sizeof(unsigned int) * nClients_);
}

unsigned int Server::getClientIp(unsigned int clientNo) {
	return clientIps_[clientNo].host;
}

unsigned int Server::getClientPort(unsigned int clientNo) {
	return clientIps_[clientNo].port;
}

unsigned int Server::getNActiveSockets(unsigned int timeout) {
	if (nClients_ == 0) {
		return 0;
	}
	signed int nReady = SDLNet_CheckSockets(socketSet_, timeout);
	if (nReady == -1) {
		std::cout << "ERROR: SDLNet_CheckSockets: " << SDLNet_GetError()
		          << std::endl;
		exit(1);
	}
	return nReady;
}

unsigned int Server::receiveData(void* data, unsigned int clientNo,
                                 unsigned int maxLength) {
	assert(data != NULL);
	if (SDLNet_SocketReady(clientSockets_[clientNo]) != 0) {
		signed int dataReceived = SDLNet_TCP_Recv(clientSockets_[clientNo],
		                                          data, maxLength);
		if (dataReceived == -1) {
			std::cout << "ERROR: SDLNet_TCP_Recv: " << SDLNet_GetError()
			          << std::endl;
			removeClient(clientNo);
			return -1;
		} else if (dataReceived == 0) {
			removeClient(clientNo);
			return -1;
		} else {
			return dataReceived;
		} 
	} 
	return 0;
}

// Watch it, this returns -1 on fail.
unsigned int Server::sendData(const void* data, unsigned int bytes, 
		unsigned int clientNo) {
	unsigned int sent = SDLNet_TCP_Send(clientSockets_[clientNo], data, bytes);
	if (sent == bytes) {
		return sent;
	} else {
		removeClient(clientNo);
		return -1;
	}
}

#define BUFFER_SIZE 256

// This should be a latency-proof method for receiving messages from clients.
void Server::getMessages() {
	unsigned int client; 
	unsigned int received;
	while (getNActiveSockets(0) != 0) {
		for (client = 0; client < getNClients(); ++client) {
			if (clientBuffers_[client] == NULL) {
				clientBuffers_[client] = (char*) malloc(BUFFER_SIZE);
				if (clientBuffers_[client] == NULL) {
					// OH NOES!!!!!
					std::cout << "memory allocation failed.\n";
					exit(1);
				}
			}
			for (;; ++clientMessageLens_[client]) {
				// Check to make sure the message won't overflow the buffer.
				if (clientMessageLens_[client] >= BUFFER_SIZE) {
					std::cout<<"Message too long. Disconnecting client.\n";
					removeClient(client);
					break;
				}
				received = receiveData(&(clientBuffers_[client][clientMessageLens_[client]]), client, 1);

				// If no data was received, break, the current buffer is kept
				// so that it can continue to be filled again later.
				if (received == 0) {
					break;
				}
				// Connection failed.. break.
				if (received == (unsigned int)-1) {
					break;
				}
				// Is it the end of the message?
				if (clientBuffers_[client][clientMessageLens_[client]] == '\0') {
					std::cout << clientBuffers_[client] << "\n";
					parseMessage(clientBuffers_[client], BUFFER_SIZE, client);
					free(clientBuffers_[client]);
					clientBuffers_[client] = NULL;
					clientMessageLens_[client] = 0;
					break;
				}
			}
		}
	}
}
