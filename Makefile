include Makefile.inc

CLIENT_DIR = client
SERVER_DIR = server

all:
	@cd $(SERVER_DIR) ; make all
	@cd $(CLIENT_DIR) ; make all

clean:
	@cd $(SERVER_DIR) ; make clean
	@cd $(CLIENT_DIR) ; make clean

run:
	@cd $(SERVER_DIR) ; make run &
	@cd $(CLIENT_DIR) ; make run
